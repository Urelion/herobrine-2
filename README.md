# Herobrine 2 #

----

[Herobrine Home Page](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=1) | [Downloads](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=2) | [Changelog](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=5) | [Configuration](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=4) | [Commands & Permissions](https://www.theprogrammersworld.net/Herobrine/commandsPermissions.php) | [Internal Bug Reporting](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=24)

----

**[Download latest version now.](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=9) A complete listing of all versions of Herobrine is available under the "[Plugin Downloads](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=10)" tab.**

Herobrine 2 is a derivative of the [Herobrine AI](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=11) plugin for Bukkit, originally written by Bukkit plugin developer [Jakub1221](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=12).
 
### How do I contribute? ###

To contribute to the project, fork the Herobrine 2 repository, make whatever changes you desire to the code, and submit a pull request. I will review your changes and merge them if they are acceptable. Any changes, whether they are bug fixes or new features, are welcome.

The recommended IDE for working on this project is [Eclipse](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=15).

### Contribution guidelines ###

Pull requests that do not provide detail on what changes were made to the code will be denied without a review. Please provide adequate information on the changes you made to the code.

### Building/Compiling ###

To build this project you will need:

- Java JDK (version 17 or newer)
- Git

To build the project:

1. Clone this repository.
2. Run `gradlew build` inside the root directory of the cloned repository.
3. The jar file will be in `build/libs/Herobrine 2.jar`.

Voila!